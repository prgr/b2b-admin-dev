﻿/*
Copyright 2011 - 2013 Mercatum
http://mercatum.pl/
*/

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) == 0;
    };
}

var saveCmd = {
    modes : { wysiwyg:1 },
    exec : function( editor ) {
		 var title = $(".title").html();
		 var value= $(".value").html();
		 var no = $("#ContentNo").val();
		 var description = $(".description").html();
		 var contenttypeid = $("#ContentTypeId").val();
		$.ajax({
        url: '/AdminContent/AjaxUpdateContent',
        type: 'POST',
        async: true,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({ No: no, title: title, value: value,description:description,contenttypeid: contenttypeid }),
        success: function (data) {
		    if (data.startsWith("Błąd"))
		    {
		        alert(data);
		    }
		else
		{
		        save = true;
		        window.location = "/"+data+".html";
		    
		}
        },
        error: function (e, f, g) {
            alert('Błąd zapisu danych   :' + e + f + g);
        }
    });

		 
   }

}
CKEDITOR.plugins.add('ajaxsave',  {    

    init:function(editor) {

	
        var pluginName = 'ajaxsave';
        var command = editor.addCommand(pluginName,saveCmd);
        command.modes = {wysiwyg:1 };   

        editor.ui.addButton('ajaxsave', {
            label: 'Zapisz zmiany',
            command: pluginName,
			icon: this.path + 'save.png'
            
        });
    }
});