﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    config.language = 'pl';
    // config.uiColor = '#AADC6E';

    //config.skin = 'BootstrapCK-Skin';
    //config.contentsCss = '/Content/themes/base/editor.css';
	config.extraPlugins = "ajaxsave";
    config.toolbar = 'inline';
	
	config.floatSpacePinnedOffsetY = 35;
    var browserPath = '/ckeditor/filemanager/browser/default/browser.html?Connector=';
    browserPath = '/AdminContent/FileBrowser?d=f';
    config.filebrowserBrowseUrl = browserPath;
    config.filebrowserImageBrowseUrl = config.filebrowserBrowseUrl + '&Type=Images&id=' + $("#ContentNo").val();
    config.filebrowserFlashBrowseUrl = config.filebrowserBrowseUrl + '&Type=Flash&id=' + $("#ContentNo").val();

    config.autoParagraph = false;
    var uploadPath = '/AdminContent/UploadFile?d=f';
    config.filebrowserImageUploadUrl = uploadPath + '&type=Images&id=' + $("#ContentNo").val();
    config.filebrowserFlashUploadUrl = uploadPath + '&type=Flash&id=' + $("#ContentNo").val();

    config.toolbar_mercatum =
    [

        ['Source'],
        ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
        ['Undo', 'Redo', 'RemoveFormat'],

        ['Bold', 'Italic', 'Underline', 'Strike', '-'],
        ['NumberedList', 'BulletedList', '-'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink', 'Anchor'],
        ['Image', 'Flash', 'Table', 'Smiley', 'SpecialChar', 'PageBreak'],

        ['Styles', 'Format', 'FontSize'],
        ['TextColor', 'BGColor'],

    ];

	
	    config.toolbar_inline =
    [
		['ajaxsave'],
        ['PasteFromWord','Undo', 'Redo', 'RemoveFormat'],
        ['Bold', 'Italic', 'Underline', 'Strike', '-'],
        
        ['NumberedList', 'BulletedList', '-'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
'/',
        ['Link', 'Unlink', 'Anchor'],
        ['Image', 'Table',  'SpecialChar'],

        ['Styles', 'Format', 'FontSize'],
        ['TextColor'],
    
    ];


    config.toolbar_newsletter =
    [

        ['Source'],
        ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
        ['Undo', 'Redo', 'RemoveFormat'],
		
        ['Bold', 'Italic', 'Underline', 'Strike', '-'],
        ['NumberedList', 'BulletedList', '-'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink', 'Anchor'],
        ['Image', 'Table', 'Smiley', 'SpecialChar', 'PageBreak'],

        ['Styles', 'Format', 'FontSize'],
        ['TextColor', 'BGColor'],

    ];


};


CKEDITOR.editor.prototype['popup'] = function (url, width, height, options) {
    width = width || '80%';
    height = height || '70%';

    if (typeof width == 'string' && width.length > 1 && width.substr(width.length - 1, 1) == '%')
        width = parseInt(window.screen.width * parseInt(width, 10) / 100, 10);

    if (typeof height == 'string' && height.length > 1 && height.substr(height.length - 1, 1) == '%')
        height = parseInt(window.screen.height * parseInt(height, 10) / 100, 10);

    if (width < 640)
        width = 640;

    if (height < 420)
        height = 420;

    var top = parseInt((window.screen.height - height) / 2, 10),
            left = parseInt((window.screen.width - width) / 2, 10);

    options = (options || 'location=no,menubar=no,toolbar=no,dependent=yes,minimizable=no,modal=yes,alwaysRaised=yes,resizable=yes,scrollbars=yes') +
            ',width=' + width +
            ',height=' + height +
            ',top=' + top +
            ',left=' + left;

    var popupWindow = window.open('', null, options, true);

    // Blocked by a popup blocker.
    if (!popupWindow)
        return false;

    try {
        // Chrome 18 is problematic, but it's not really needed here (#8855).
        var ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf(' chrome/20') == -1) {
            popupWindow.moveTo(left, top);
            popupWindow.resizeTo(width, height);
        }
        popupWindow.focus();
        popupWindow.location.href = url;
    }
    catch (e) {
        popupWindow = window.open(url, null, options, true);
    }

    return true;
}


