import { HelperModule } from './../helpers';

const helpers = new HelperModule.HelpersMethods();

export module CarouselModule {
    export interface CarouselModule{
            //
    }
    export class Carousel {
        carousel(banners: any, set_active?: number): void{
            //get current image
            let carousel_img = (<HTMLImageElement>document.querySelector('.banner-holder img'));
            let carousel_text = (<HTMLParagraphElement>document.querySelector('.banner-holder .banner-text'));
            //set active image as1 to bypass the first image
            let active = 1;
            let banners_interval = setInterval(function(){
                if(window.location.pathname == '/customer/dashboard'){
                    this.removeLastActiveNav(active);
                    //get current span NAV
                    let current_span_nav = (<HTMLSpanElement>document.querySelector('.bottom-navigation span[data-banner-id="' + active + '"]'));
                    let previous_span_nav = (<HTMLElement>current_span_nav.previousSibling);
                    if(!helpers.isNull(previous_span_nav.classList)){
                        previous_span_nav.classList.remove('banner-active');
                    }
                    else
                        current_span_nav.classList.remove('banner-active');
                    carousel_img.setAttribute('src', `../../assets/images/banners/${banners[active].name}.jpg`);
                    carousel_text.innerHTML = banners[active].text;
                    current_span_nav.classList.add('banner-active');
                    active++;
                    if (active == banners.length){
                        active = 0;
                        //if(current_span_nav.nextSibling.nodeName.toLowerCase() !== 'span')
                            //current_span_nav.classList.remove('banner-active');
                    }
                }
                else{
                    clearInterval(banners_interval);
                    return;
                }
                }.bind(this), 3000);
        }
        activeFirstBanner(banner: any): void{
            let first_image = (<HTMLSpanElement>document.querySelector('.banner-holder img:first-CHIld'));
            let first_image_text = (<HTMLParagraphElement>document.querySelector('.banner-holder .banner-text'));
            let first_image_nav = (<HTMLSpanElement>document.querySelector('.bottom-navigation span[data-banner-id="' + 0 + '"]'));
            first_image.setAttribute('src', `../../assets/images/banners/${banner.name}.jpg`);
            first_image_text.innerHTML = banner.text;
            first_image_nav.classList.add('banner-active');
        }
        removeLastActiveNav(active: number): void{
            if(active == 0){
                let last_span_nav = (<HTMLSpanElement>document.querySelector('.bottom-navigation span:last-child'));
                if(last_span_nav.classList.contains('banner-active'))
                    last_span_nav.classList.remove('banner-active');
            }
            else
                return;
        }
        changeBanner(banners: any, data_banner_id: number): void{
            //this.carousel(banners, data_banner_id);
        }
    }
}