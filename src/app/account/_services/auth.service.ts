import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
    public token: string;

    constructor(private http: Http, private router: Router) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

//     register(email:string,password:string,firstName:string,lastName:string): Observable {    
//         let headers = new HttpHeaders()
//             .set('login', 'username')
//             .set('password', 'password');
//         let options = new RequestOptions({ headers: headers });
//         let body=JSON.parse(localStorage.getItem('currentUser'));          
//         return this.http.post('/localhost:3000/login',body,
//         {headers})
//         .map(data =>{ 
//              return data;      
//          });
//   }

        hero(){
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('login', `username`);
            headers.append('password', 'password');
            console.log('hero');
            let options = new RequestOptions({ headers: headers });
            var body = 'login=username&password=password';
            return this.http
                .post('http://localhost:3000/login', body, options).subscribe(data => {
                    alert('ok');
              }, error => {
                  console.log('q');
              });
        }

        alfa(){
            var options = new RequestOptions({
                headers: new Headers({
                  'login': 'username',
                  'password': 'password'
                })
              });
          
              this.http.post('localhost:3000/login', '', options)
                .map(resp => console.log(console.log(resp)));
            }

            ranger(username: string, password: string) {
                let body = '';
                let user = btoa(`${username}:${password}`);
                //var headers = new Headers();
                //headers.append('Content-Type', 'application/x-www-form-urlencoded');
                const headers = new Headers();
                //headers.append('login', 'username');
                //headers.append("password", "password");
                //headers.append('Content-Type', 'application/x-www-form-urlencoded');
                //headers.append('Authorization', 'username:password');
                headers.append('Authorization', 'Basic ' + user);
                //headers.append('Content-Type', 'application/x-www-form-urlencoded');
                //headers.append('access-control-expose-headers', 'x-total-count');
                //headers.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                this.http
                    .post('http://localhost:3000/login',
                    null, {
                        headers: headers
                    })
                    .subscribe(res => {
                        //let hero = res.json();
                        let charlie = res.headers;
                        console.log(charlie);
                        console.log(headers.get('token'));
                        this.router.navigate(['/admin/dashboard']);
                    }, error => {
                        console.log(error);
                    });
            }

            redPanda(username: string, password: string){
                let user = btoa(`${username}:${password}`);
                const headers = new Headers();
                let requestOptions = new RequestOptions({ headers: headers });       
                headers.append('Authorization', 'Basic ' + user);
                headers.append('Content-Type', 'application/x-www-form-urlencoded');

                return this.http.post('http://localhost:3000/login', '', requestOptions)
                .map((response: Response) => {`enter code here`
                     console.log(response);
                 });
            }


            panda(username: string, password: string) {    
                let user = btoa(`${username}:${password}`);
                const headers = new Headers();
                headers.append('Authorization', 'Basic ' + user);
                    return this.http.post('http://localhost:3000/login', null, { headers: headers })
                    .toPromise()
                    .then(res => {
                        let a = res;
                        console.log(a.headers.get('token'))
                    })
                    .catch(err => console.log(err));
            }

            /*
            mag(){
                var body = 'login=username&password=password';
                let headers = new Headers({'token': 'token'});  
                headers.append('Authorization','Bearer ')
                let options = new RequestOptions({headers: headers});
                this.http
                .post('http://localhost:3000/login',
                body, {
                  headers: headers
                }).subscribe(data => {
                    alert('ok');
              }, error => {
                  console.log(JSON.stringify(error.json()));
              });
            }
            */

        /*
        charlie(): Observable<any> {
            let data = JSON.stringify({ login: 'username', password: 'password' })
            let username: string = 'login';
            let password: string = 'password';
            let headers: Headers = new Headers();
            headers.append('login', 'username'); 
            headers.append('password', 'password');
            return this.http.post('/http://localhost:3000/login', data, {headers: headers}).map((response: Response) => {
                console.log('p');
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                    // return true to indicate successful login
                    console.log('true')
                    return '';
                } else {
                    console.log('false')
                    // return false to indicate failed login
                    return '';
                }
            });
          }
          */


          /*

    login(username: string, password: string): Observable<string> {
        console.log('hero');
        let headers = new Headers({ 'login': 'username', 'password': 'password' });
            let options = new RequestOptions({ headers: headers });
            return this.http.post('/http://localhost:3000/login', headers).map((response: Response) => {
                console.log('p');
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                    // return true to indicate successful login
                    console.log('true')
                    return '';
                } else {
                    console.log('false')
                    // return false to indicate failed login
                    return '';
                }
            });
        } 

        */

    /*
    login(username: string, password: string): Observable<boolean> {
        return this.http.post('/localhost:3000/login', JSON.stringify({ username: username, password: password }))
            .map((response: Response) => {
                console.log('p');
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }
    */

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
    }
}