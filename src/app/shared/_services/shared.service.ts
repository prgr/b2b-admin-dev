import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
    public language: any | null;
    constructor() { }

    getUsedLanguage(){
        //get local lang
        const local_lang = localStorage.getItem('lang');
        //check if language is already choosen if not set default language to pl
        return local_lang !== null || undefined ? local_lang : 'pl';
    }
}