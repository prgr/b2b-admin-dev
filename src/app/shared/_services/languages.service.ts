import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LanguagesService {
    public language: any | null;
    constructor(private http:Http) {

    }
    getLanguage(lang: string){
        this.http.get(`assets/data/languages/${lang}.json`)
        .subscribe(res => {
            const arr = res.json();
            arr.forEach(el => {
                this.language.push(el);
            });
            this.language.push(...res.json())
        });
    }
    getApplications(lang: string): Promise<any[]> {
        return this.http
        .get(`assets/data/languages/${lang}.json`)
        .toPromise()
        .then(response => response.json() as any[]);
    }
    setLanguage(lang: string): Promise<JSON>{
        return this.http.get(`assets/data/languages/${lang}.json`).map(response => response.json() as JSON).toPromise();
    }
    hero(lang: string){
      this.http.get(`assets/data/languages/${lang}.json`)
      .map(res => res.json())
      .subscribe(
        json => {
          this.language = json;
        },
        error => console.log(error),
      );
    }
    getUsedLanguage(){
        //get local lang
        const local_lang = localStorage.getItem('lang');
        //check if language is already choosen if not set default language to pl
        return local_lang !== null || undefined ? local_lang : 'pl';
    }
}