import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MdSidenavModule, MdMenuModule, MdToolbarModule, MdGridListModule, MdCardModule, MdListModule, MdIconModule, MdInputModule, MdButtonModule, MdCheckboxModule, MdTooltipModule, MdAutocompleteModule, MdOptionModule, MdDialogModule, MdSlideToggleModule, MdTabsModule, MdSnackBarModule, MdButtonToggleModule, MatSelectModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MdSidenavModule,
    MdMenuModule,
    MdToolbarModule,
    MdGridListModule,
    MdCardModule,
    MdListModule,
    MdIconModule,
    MdInputModule,
    MdButtonModule,
    MdCheckboxModule,
    MdTooltipModule,
    MdOptionModule,
    MdDialogModule,
    MdSlideToggleModule,
    MdTabsModule,
    MdSnackBarModule,
    MdButtonToggleModule,
    MatSelectModule
  ],
  declarations: [

  ],
  providers: [],
  exports: [
    CommonModule,
    MdSidenavModule,
    MdMenuModule,
    MdToolbarModule,
    MdGridListModule,
    MdCardModule,
    MdListModule,
    MdIconModule,
    MdInputModule,
    MdButtonModule,
    MdCheckboxModule,
    MdTooltipModule,
    MdOptionModule,
    MdDialogModule,
    MdSlideToggleModule,
    MdTabsModule,
    MdSnackBarModule,
    MdButtonToggleModule,
    MatSelectModule
  ]
})

export class MaterialDesignModule { }