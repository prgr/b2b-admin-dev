import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguagesService } from '../../../../shared/_services/languages.service';
import { SharedService } from '../../../../shared/_services/shared.service';
import { PriceList } from '../_models/price-list';
import { PriceListService } from '../_services/price-list.service';

@Component({
    templateUrl: 'admin.modify-price-list-configuration.compontent.html'
})

export class AdminModifyPriceListConfigurationComponent {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
        private priceListService: PriceListService,
        private route: ActivatedRoute
    ) { }
    public priceList: PriceList;
    private priceListID: number;

    ngOnInit() {
        //language
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        this.route.params.subscribe(
            (params: any) => {
                this.priceListID = params['id']
                //get static page
                if(this.priceListID !== undefined)
                    this.priceList = this.priceListService.getPriceListById(this.priceListID);
            }
        )
     }
}