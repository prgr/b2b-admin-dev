import { Injectable } from '@angular/core';
import { PriceList } from '../_models/price-list';

@Injectable()
export class PriceListService {
    public priceLists: PriceList[] = [
        new PriceList(1, 'hero', 'EUR'),
        new PriceList(2, 'alfa', 'USD'),
        new PriceList(3, 'ranger', 'PL'),
    ]
    constructor() { }

    getPriceListById(id: number): PriceList{
        return this.priceLists.find(x => x.priceListID == id);
    }
}