export class PriceList{
    
    constructor(
        public priceListID: number,
        public name: string,
        public currency: string,
    ){}
    }