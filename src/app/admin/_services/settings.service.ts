import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable, Observer} from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class SettingsService {
    public globalSettings: any;
    constructor(private http:Http) {
        
    }
    setSettings(){
        return this.http.get('assets/data/settings/settings.json').map(res => res.json()).subscribe(
          data => {
            //store settings
            this.globalSettings = data;
          },
          err => {
              alert(`Error podczas wczytywania ustawien: ${err}. Prosimy skontaktowac sie z administracja.`)
              try {
                  //
              } catch (error) {
                  console.log(error);
              }
              //onsole.log(`Error: ${err}`);
          }
      )
        /*
        .subscribe(v => {
            const hero: Settings[] = [];
            v.settings.forEach(set => {
                let new_sets: Settings;
                for (var key in set) {
                    if (!Object.prototype.hasOwnProperty.call(set, key)) continue;{
                        set = set[key];
                        new_sets = new Settings(set.type, set.value, set.description, set.group);
                        break;
                    }
                }
                const new_key_set = new Key('qqq',new_sets);
                //const new_set = new Key(set.key, set.type, set.value, set.description, set.group);
                this.settings.push(new_key_set)
            });
            console.log(this.settings[0]);
        },
        */
    }
    getSettings(){
        return this.http.get('assets/data/settings/settings.json').map(response => response.json() as JSON).toPromise();
    }
    getSettingsHero(){
        this.http.get('assets/data/settings/settings.json').map(response => response.json()).subscribe( data =>{
            return data;  
        });
    }
}

// https://stackoverflow.com/questions/14020726/remove-json-root