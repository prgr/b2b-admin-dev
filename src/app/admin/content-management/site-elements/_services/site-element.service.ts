import { Injectable } from '@angular/core';
import { SiteElement } from '../_models/site-element';

@Injectable()
export class SiteElementService {
    public siteElements: SiteElement[] = [
        new SiteElement(1, 'alfa', '<h2>alfa h2 tag</h2>'),
        new SiteElement(2, 'mag', 'mag content')
    ]
    constructor() { }

    getSiteElementById(id: number): SiteElement{
        return this.siteElements.find(x => x.siteElementID == id);
    }
}