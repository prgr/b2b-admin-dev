export class SiteElement{

constructor(
    public siteElementID: number,
    public title: string,
    public content: string,
){}
}