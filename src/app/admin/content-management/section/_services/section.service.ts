import { Injectable } from '@angular/core';
import { Section } from '../_models/section';

@Injectable()
export class SectionService {
    public sections: Section[] = [
        new Section(1, 1, 'section 1', '<h2>hero h2 tag</h2>', false, false, true, false, false, false, 'section-hero', '', '', ''),
        new Section(2, 2, 'section 2', '<h2>alfa h3 tag</h2>', false, false, true, false, false, false, 'section-alfa', '', '', ''),
        new Section(3, 3, 'section 3', '<h2>charlie h4 tag</h2>', false, false, true, false, false, false, 'section-charlie', '', '', ''),
        new Section(4, 4, 'section 4', '<h2>ranger h5 tag</h2>', false, false, true, false, false, false, 'section-ranger', '', '', '')
    ]
    constructor() { }

    getSectionById(id: number): Section{
        return this.sections.find(x => x.sectionID == id);
    }
}