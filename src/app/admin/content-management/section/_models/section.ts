export class Section{
    
    constructor(
        public sectionID: number,
        public parentID: number,
        public name: string,
        public content: string,
        public description: boolean,
        public metaTags: boolean,
        public url: boolean,
        public date: boolean,
        public tags: boolean,
        public sort: boolean,
        public slug: string,
        public template: string,
        public templateList: string,
        public menu: string
    ){}
    }