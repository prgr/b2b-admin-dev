import { Injectable } from '@angular/core';
import { StaticPage } from '../_models/static-page';

@Injectable()
export class StaticPageService {
    public staticPages: StaticPage[] = [
        new StaticPage(1, 'hero', '<h2>herooo h2 tag</h2>'),
        new StaticPage(2, 'charlie', 'charlie content')
    ]
    constructor() { }

    getStaticPageById(id: number): StaticPage{
        return this.staticPages.find(x => x.staticPageID == id);
    }
}