export class StaticPage{

constructor(
    public staticPageID: number,
    public title: string,
    public content: string,
){}
}