import { Injectable } from '@angular/core';
import { News } from '../_models/news';

@Injectable()
export class NewsService {
    public news: News[] = [
        new News(1, 'news hero', '<h2>news hero h2 tag</h2>'),
        new News(2, 'news mag', 'mag content')
    ]
    constructor() { }

    getNewsById(id: number): News{
        return this.news.find(x => x.newsID == id);
    }
}