import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../shared/_services/shared.service';
import { News } from './_models/news';
import { NewsService } from './_services/news.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    templateUrl: 'admin.news-content-management.component.html'
})

export class AdminNewsContentManagementComponent implements OnInit {
    constructor(
        private sharedService: SharedService,
        private newsService: NewsService,
        private translate: TranslateService,
    ) { }

    ngOnInit(){

    }

}