export class News{

constructor(
    public newsID: number,
    public title: string,
    public content: string,
){}
}