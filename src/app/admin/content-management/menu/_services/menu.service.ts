import { Injectable } from '@angular/core';
import { Menu } from '../_models/menu';

@Injectable()
export class MenuService {
    public menus: Menu[] = [
        new Menu(1, 'hero menu', '/hero.html'),
        new Menu(2, 'charlie menu', '/charlie.html')
    ]
    constructor() { }

    getMenuById(id: number): Menu{
        return this.menus.find(x => x.menuID == id);
    }
}