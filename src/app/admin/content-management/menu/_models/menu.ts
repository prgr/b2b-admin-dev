export class Menu{

constructor(
    public menuID: number,
    public name: string,
    public link: string,
){}
}