import { Injectable } from '@angular/core';
import { Banner } from '../_models/banner';

@Injectable()
export class BannerService {
    public news: Banner[] = [
        new Banner(1, 'banner charlie', '<h2>alfa hero h2 tag</h2>'),
        new Banner(2, 'banner alfa', 'ALFA content')
    ]
    constructor() { }

    getBannerById(id: number): Banner{
        return this.news.find(x => x.bannerID == id);
    }
}