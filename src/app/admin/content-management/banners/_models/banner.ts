export class Banner{

constructor(
    public bannerID: number,
    public title: string,
    public content: string,
){}
}