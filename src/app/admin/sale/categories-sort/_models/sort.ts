export class Sort{
    
    constructor(
        public sortID: number,
        public categoryName: string,
        public position: number,
    ){}
    }