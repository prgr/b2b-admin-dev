import { Injectable } from '@angular/core';
import { Sort } from '../_models/sort';

@Injectable()
export class CategoriesSortService {
    public categoriesSort: Sort[] = [
        new Sort(1, 'hero', 1),
        new Sort(2, 'alfa', 2),
        new Sort(3, 'ranger', 3),
    ]
    constructor() { }
}