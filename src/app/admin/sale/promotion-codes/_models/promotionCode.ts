export class PromotionCode{
    
    constructor(
        public promotionCodeID: number,
        public campainName: string,
        public code: string,
        public productsCodes: string,
        public manufacturersSymbols: string,
        public discount: number,
        public active: boolean,
        public usedCount: number,
    ){}
    }