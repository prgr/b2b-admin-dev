import { Injectable } from '@angular/core';
import { PromotionCode } from '../_models/promotionCode';

@Injectable()
export class PromotionCodesService {
    public promotionCodes: PromotionCode[] = [
        new PromotionCode(1, 'hero', 'hero#1', '', '', 50, true, 1),
        new PromotionCode(1, 'alfa', 'alfa#1', '', '', 50, true, 1),
        new PromotionCode(1, 'ranger', 'ranger#1', '', '', 50, true, 1),
        new PromotionCode(1, 'charlie', 'CHARlie#1', '', '', 50, true, 1),
    ]
    constructor() { }

    getPromotionCodeById(id: number): PromotionCode{
        return this.promotionCodes.find(x => x.promotionCodeID == id);
    }
}