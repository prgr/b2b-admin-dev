import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../../shared/_services/shared.service';
import { TraderService } from '../_services/trader.service';
import { Trader } from '../_models/trader';

@Component({
    templateUrl: 'admin.modify-trader-sale.compontent.html'
})

export class AdminModifyTraderSaleComponent {
        constructor(
            private translate: TranslateService,
            private sharedService: SharedService,
            private route: ActivatedRoute,
            private traderService: TraderService,
        ) { }
        public trader: Trader;
        private traderID: number;
    
        ngOnInit() {
            //language
            let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
            this.translate.use(lang);
            this.route.params.subscribe(
                (params: any) => {
                    this.traderID = params['id']
                    //get static page
                    if(this.traderID !== undefined)
                        this.trader = this.traderService.getTraderById(this.traderID);
                }
            )
         }
    
         modifyTrader(trader: Trader): void{
            
         }
}