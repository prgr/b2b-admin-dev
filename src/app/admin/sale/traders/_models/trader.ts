export class Trader{
    
    constructor(
        public traderID: number,
        public name: string,
        public surname: string,
        public email: string,
        public phone: string,
        public skype: string,
        public password: string
    ){}
}