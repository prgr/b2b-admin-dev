import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../shared/_services/shared.service';
import { HelperModule } from '../../../../assets/js/helpers';

@Component({
    templateUrl: 'admin.modify-product-sale.component.html'
})

export class AdminModifyProductSaleComponent implements OnInit {
    constructor(private sharedService: SharedService, private translate: TranslateService) { }
    helpers = new HelperModule.HelpersMethods();

    ngOnInit() {
        //initial multi languages
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
     }

     uploadImagesPreview(ul: HTMLUListElement, input: HTMLInputElement): void{
        this.helpers.uploadImagePreview(ul, input);
     }
}