import { Injectable } from '@angular/core';
import { Stock } from '../_models/Stock';

@Injectable()
export class StocksService {
    public promotionCodes: Stock[] = [
        new Stock(1, 'hero', 'hero', 'desc', '', 0, true),
        new Stock(1, 'alfa', 'alfa', 'desc', '', 0, false),
        new Stock(1, 'ranger', 'ranger', 'desc', '', 0, false),
    ]
    constructor() { }

    getStockById(id: number): Stock{
        return this.promotionCodes.find(x => x.stockID == id);
    }
}