import { Injectable } from '@angular/core';
import { Loyalty } from '../_models/Loyalty';

@Injectable()
export class LoyaltyService {
    public loyalties: Loyalty[] = [
        new Loyalty(1, 'hero', '<h1>hero</h1>', 50, true),
        new Loyalty(2, 'alfa', '<h1>alfa</h1>', 50, true),
        new Loyalty(3, 'ranger', '<h1>ranger</h1>', 50, true),
    ]
    constructor() { }

    getLoyaltyById(id: number): Loyalty{
        return this.loyalties.find(x => x.loyaltyID == id);
    }
}