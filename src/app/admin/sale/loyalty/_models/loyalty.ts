export class Loyalty{
    
    constructor(
        public loyaltyID: number,
        public name: string,
        public content: string,
        public points: number,
        public visible: boolean
    ){}
    }