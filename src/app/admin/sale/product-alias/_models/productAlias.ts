import { Product } from '../../products/_models/product';

export class ProductAlias{
    
    constructor(
        public productAliasID: number,
        public product: Product,
        public symbol: string,
        public category: string,
        public name: string,
        public description: string
    ){}
}