import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../../shared/_services/shared.service';
import { ProductAliasService } from '../_services/product-alias.service';
import { ProductAlias } from './/../_models/productAlias';

@Component({
    templateUrl: 'admin.modify-alias-sale.component.html'
})

export class AdminModifyAliasSaleComponent {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
        private route: ActivatedRoute,
        private productAliasService: ProductAliasService,
    ) { }
    public productAlias: ProductAlias;
    private productAliasID: number;

    ngOnInit() {
        //language
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        this.route.params.subscribe(
            (params: any) => {
                this.productAliasID = params['id']
                //get static page
                if(this.productAliasID !== undefined)
                    this.productAlias = this.productAliasService.getProductAliasById(this.productAliasID);
            }
        )
     }

     modifyProductAlias(productAlias: ProductAlias): void{
        
     }
}