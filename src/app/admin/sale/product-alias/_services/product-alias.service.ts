import { Injectable } from '@angular/core';
import { ProductAlias } from '../_models/productAlias';

@Injectable()
export class ProductAliasService {
    public productAliases: ProductAlias[] = [
        new ProductAlias(1, null, 'hero', 'hero cat', 'product 1', 'desc'),
        new ProductAlias(2, null, 'alfa', 'alfa  cat', 'product 2', 'desc'),
        new ProductAlias(3, null, 'ranger', 'ranger cat', 'product 3', 'desc'),
    ]
    constructor() { }

    getProductAliasById(id: number): ProductAlias{
        return this.productAliases.find(x => x.productAliasID == id);
    }
}