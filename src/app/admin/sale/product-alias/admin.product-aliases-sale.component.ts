import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../shared/_services/shared.service';
import { ProductAliasService } from './_services/product-alias.service';
import { ProductAlias } from './_models/productAlias';

@Component({
    templateUrl: 'admin.product-aliases-sale.component.html'
})

export class AdminProductAliasesSaleComponent {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
        private route: ActivatedRoute,
        private productAliasService: ProductAliasService,
    ) { }
    ngOnInit() {
        //language
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
     }
}